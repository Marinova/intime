import { StorageService } from './../core/storage.service';


describe('StorageService', () => {

    it('should be truthy', () => {
        // const fixture = TestBed.createComponent(AppComponent);
        // const app = fixture.debugElement.componentInstance;
        const storageService = new StorageService();

        expect(storageService).toBeTruthy();
    });

    it('should call get Item method', () => {
        const storageService = new StorageService();

        spyOn(storageService, 'getItem')
        storageService.getItem('test');

        expect(storageService.getItem).toHaveBeenCalled();
    });

    // it('should call setItem method', () => {
    //     const storageService = new StorageService();

    //     spyOn(storageService, 'setItem')
    //     storageService.setItem('test', 'test');

    //     expect(localStorage.getItem('test')).toContain({});
    // });

    it('should call setItem method', () => {
        const storageService = new StorageService();

        spyOn(storageService, 'setItem')
        storageService.setItem('test', 'test');

        expect(storageService.setItem).toHaveBeenCalled();
    });

    it('should call removeItem method', () => {
        const storageService = new StorageService();

        spyOn(storageService, 'removeItem')
        storageService.removeItem('test');

        expect(storageService.removeItem).toHaveBeenCalled();
    });

    it('should call clear method', () => {
        const storageService = new StorageService();

        spyOn(storageService, 'clear')
        storageService.clear();

        expect(storageService.clear).toHaveBeenCalled();
    });

    //   it(`should have as title 'client'`, () => {
    //     const fixture = TestBed.createComponent(AppComponent);
    //     const app = fixture.debugElement.componentInstance;
    //     expect(app.title).toEqual('client');
    //   });

    //   it('should render title in a h1 tag', () => {
    //     const fixture = TestBed.createComponent(AppComponent);
    //     fixture.detectChanges();
    //     const compiled = fixture.debugElement.nativeElement;
    //     expect(compiled.querySelector('h1').textContent).toContain('Welcome to client!');
    //   });
});
