import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthRouteActivatorService } from '../core/guards/auth-route-activator.service';
import { NavbarService } from '../core/navbar.service';

describe('NavbarService', () => {

  it('should be created', () => {
    const nav = new NavbarService()

    expect(nav).toBeTruthy();
  });

  it('should be passed false', () => {
    const nav = new NavbarService()

    expect(nav.visible).toBe(false);
  });

  it('should be changed to true with show method', () => {
    const nav = new NavbarService()
    nav.show()
    expect(nav.visible).toBe(true);
  });

  it('should be called', () => {
    const nav = new NavbarService()
    spyOn(nav, 'show')
    //const spy = jasmine.createSpyObj('nav', ['show']);
    //const spy = jasmine.
    nav.show()
    expect(nav.show).toHaveBeenCalled();
  });

  it('should be called', () => {
    const nav = new NavbarService()
    spyOn(nav, 'hide')
    //const spy = jasmine.createSpyObj('nav', ['show']);
    //const spy = jasmine.
    nav.hide()
    expect(nav.hide).toHaveBeenCalled();
  });

  it('should be set to false', () => {
    const nav = new NavbarService()
    spyOn(nav, 'hide')
    //const spy = jasmine.createSpyObj('nav', ['show']);
    //const spy = jasmine.
    nav.hide()
    expect(nav.hide).toHaveBeenCalled();
    expect(nav.visible).toBe(false);
  });



});
